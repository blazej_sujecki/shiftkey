package com.shiftkey.codingchallenge.details.viewmodel

import androidx.lifecycle.ViewModel
import com.shiftkey.codingchallenge.connection.models.Shift

class DetailsViewModel(
        val shift: Shift) : ViewModel() {
}