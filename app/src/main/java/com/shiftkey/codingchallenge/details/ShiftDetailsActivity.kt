package com.shiftkey.codingchallenge.details

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.shiftkey.codingchallenge.databinding.ActivityShiftDetailsBinding
import com.shiftkey.codingchallenge.details.viewmodel.DetailsViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class ShiftDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityShiftDetailsBinding

    private val viewModel: DetailsViewModel by viewModel {
        parametersOf(intent.extras?.getSerializable(KEY_SHIFT_OBJ))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityShiftDetailsBinding.inflate(layoutInflater)
        binding.apply {
            this.viewModel = this@ShiftDetailsActivity.viewModel
            lifecycleOwner = this@ShiftDetailsActivity
        }
        setContentView(binding.root)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        const val KEY_SHIFT_OBJ = "key_shift_obj"
    }
}