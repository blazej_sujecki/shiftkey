package com.shiftkey.codingchallenge.main.viewmodel

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.shiftkey.codingchallenge.main.repository.ShiftDayRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*
import java.util.*

class MainViewModel(
        private val repository: ShiftDayRepository,
        private val savedStateHandle: SavedStateHandle) : ViewModel() {

    init {
        if (!savedStateHandle.contains(KEY_DATE)) {
            savedStateHandle.set(KEY_DATE, DEFAULT_DATE.time)
        }
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    val shifts = flowOf(
            savedStateHandle.getLiveData<Long>(KEY_DATE)
                    .asFlow()
                    .flatMapLatest { repository.fetch(Date(savedStateHandle.get<Long>(KEY_DATE)!!), pageSize = PAGE_SIZE) }
                    .cachedIn(viewModelScope)
    ).flattenMerge()

    companion object {
        const val KEY_DATE = "shift_key_date"
        val DEFAULT_DATE = Date()//now
        const val PAGE_SIZE = 10
    }
}