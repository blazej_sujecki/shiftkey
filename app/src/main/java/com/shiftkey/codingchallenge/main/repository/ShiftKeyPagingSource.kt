package com.shiftkey.codingchallenge.main.repository

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.shiftkey.codingchallenge.configuration.Configuration
import com.shiftkey.codingchallenge.connection.api.ShiftKeyApi
import com.shiftkey.codingchallenge.connection.models.ShiftDay
import retrofit2.HttpException
import java.io.IOException
import java.util.*

class ShiftKeyPagingSource(private val shiftKeyApi: ShiftKeyApi,
                           private val date: Date,
                           private val configuration: Configuration) : PagingSource<Date, ShiftDay>() {

    override suspend fun load(params: LoadParams<Date>): LoadResult<Date, ShiftDay> {
        return try {
            val data = shiftKeyApi.getShifts(date = params.key
                    ?: date, type = configuration.period, address = configuration.address, radius = configuration.range).data

            LoadResult.Page(data = data, prevKey = prevWeekIfPossible(data.firstOrNull()?.date), nextKey = nextWeek(data.lastOrNull()?.date))
        } catch (e: IOException) {
            LoadResult.Error(e)
        } catch (e: HttpException) {
            LoadResult.Error(e)
        }
    }

    private fun prevWeekIfPossible(date: Date?): Date? = prevWeek(date)?.let { if (it.after(date)) it else null }

    private fun nextWeek(date: Date?) = addWeek(date, 1)
    private fun prevWeek(date: Date?) = addWeek(date, -1)

    private fun addWeek(date: Date?, add: Int): Date? {
        if (date == null) return null
        val calendar = Calendar.getInstance().apply { time = date }
        calendar.set(Calendar.WEEK_OF_YEAR, calendar.get(Calendar.WEEK_OF_YEAR) + add)
        return calendar.time
    }

    override val keyReuseSupported: Boolean
        get() = true

    override fun getRefreshKey(state: PagingState<Date, ShiftDay>): Date? =
            state.anchorPosition?.let { anchorPosition ->
                state.closestItemToPosition(anchorPosition)?.date
            }
}