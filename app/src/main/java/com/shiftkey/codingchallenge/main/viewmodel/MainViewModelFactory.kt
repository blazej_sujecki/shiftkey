package com.shiftkey.codingchallenge.main.viewmodel

import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import com.shiftkey.codingchallenge.configuration.Configuration
import com.shiftkey.codingchallenge.connection.api.ShiftKeyApi
import com.shiftkey.codingchallenge.main.repository.ShiftDayRepositoryImpl

class MainViewModelFactory(owner: SavedStateRegistryOwner, val api: ShiftKeyApi, private val configuration: Configuration) : AbstractSavedStateViewModelFactory(owner, null) {
    override fun <T : ViewModel?> create(key: String, modelClass: Class<T>, handle: SavedStateHandle): T {
        val repo = ShiftDayRepositoryImpl(api, configuration)
        @Suppress("UNCHECKED_CAST")
        return MainViewModel(repo, handle) as T
    }
}