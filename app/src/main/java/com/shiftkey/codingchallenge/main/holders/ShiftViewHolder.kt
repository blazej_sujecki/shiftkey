package com.shiftkey.codingchallenge.main.holders

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.RecyclerView
import com.shiftkey.codingchallenge.BR
import com.shiftkey.codingchallenge.common.Navigator
import com.shiftkey.codingchallenge.connection.models.Shift
import com.shiftkey.codingchallenge.connection.models.ShiftDay
import com.shiftkey.codingchallenge.databinding.ItemShiftdayBinding
import com.shiftkey.codingchallenge.details.ShiftDetailsActivity
import java.text.SimpleDateFormat
import java.util.*


class ShiftViewHolder(private val binding: ItemShiftdayBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(viewModel: ShiftDayViewModel) {
        binding.setVariable(BR.viewModel, viewModel)
    }
}

class ShiftDayViewModel(shiftDay: ShiftDay?, navigator: Navigator) : ViewModel() {

    private val _shiftDate = MutableLiveData(shiftDay?.let { formatDate(it.date) })
    val shiftDate: LiveData<String> = _shiftDate

    private val _shifts = MutableLiveData(shiftDay?.shifts ?: emptyList())
    val shifts: LiveData<List<Shift>> = _shifts

    val shiftSelectListener = object : ShiftSelectListener {
        override fun onSelectShift(shift: Shift) {
            navigator.navigate(ShiftDetailsActivity::class.java, buildBundle = {putSerializable(ShiftDetailsActivity.KEY_SHIFT_OBJ, shift)})
        }
    }

    private fun formatDate(date: Date) = SimpleDateFormat(DATE_FORMAT, Locale.getDefault()).format(date)

    companion object {
        const val DATE_FORMAT = "yyyy-MM-dd"
    }

    interface ShiftSelectListener {
        fun onSelectShift(shift: Shift)
    }
}