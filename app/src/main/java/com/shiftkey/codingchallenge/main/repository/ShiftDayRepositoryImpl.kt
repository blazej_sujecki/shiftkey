package com.shiftkey.codingchallenge.main.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.shiftkey.codingchallenge.configuration.Configuration
import com.shiftkey.codingchallenge.connection.api.ShiftKeyApi
import java.util.*

class ShiftDayRepositoryImpl(private val api: ShiftKeyApi, private val configuration: Configuration) : ShiftDayRepository {

    override fun fetch(date: Date, pageSize: Int) =
            Pager(PagingConfig(pageSize, enablePlaceholders = true)) { ShiftKeyPagingSource(api, date, configuration) }.flow
}