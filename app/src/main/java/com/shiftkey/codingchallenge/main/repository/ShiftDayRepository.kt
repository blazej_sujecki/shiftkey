package com.shiftkey.codingchallenge.main.repository

import androidx.paging.PagingData
import com.shiftkey.codingchallenge.connection.models.Shift
import com.shiftkey.codingchallenge.connection.models.ShiftDay
import kotlinx.coroutines.flow.Flow
import java.util.*

interface ShiftDayRepository {
    fun fetch(date: Date, pageSize: Int): Flow<PagingData<ShiftDay>>
}