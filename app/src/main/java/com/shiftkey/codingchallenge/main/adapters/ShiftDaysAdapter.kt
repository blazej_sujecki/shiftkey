package com.shiftkey.codingchallenge.main.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.shiftkey.codingchallenge.common.Navigator
import com.shiftkey.codingchallenge.connection.models.ShiftDay
import com.shiftkey.codingchallenge.databinding.ItemShiftdayBinding
import com.shiftkey.codingchallenge.main.holders.ShiftViewHolder
import com.shiftkey.codingchallenge.main.holders.ShiftDayViewModel

class ShiftDaysAdapter(private val navigator: Navigator) : PagingDataAdapter<ShiftDay, ShiftViewHolder>(POST_COMPARATOR) {

    override fun onBindViewHolder(holder: ShiftViewHolder, position: Int) {
        holder.bind(ShiftDayViewModel(getItem(position), navigator))
    }

    override fun onBindViewHolder(holder: ShiftViewHolder, position: Int, payloads: MutableList<Any>) {
        onBindViewHolder(holder, position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShiftViewHolder {
        val binding: ItemShiftdayBinding = ItemShiftdayBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ShiftViewHolder(binding)
    }

    companion object {
        val POST_COMPARATOR = object : DiffUtil.ItemCallback<ShiftDay>() {
            override fun areContentsTheSame(oldItem: ShiftDay, newItem: ShiftDay): Boolean =
                    oldItem == newItem

            override fun areItemsTheSame(oldItem: ShiftDay, newItem: ShiftDay): Boolean =
                    oldItem.date == newItem.date

        }
    }
}