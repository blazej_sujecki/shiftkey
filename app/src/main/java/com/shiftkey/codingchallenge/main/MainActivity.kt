package com.shiftkey.codingchallenge.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.shiftkey.codingchallenge.common.AndroidNavigator
import com.shiftkey.codingchallenge.databinding.ActivityMainBinding
import com.shiftkey.codingchallenge.main.adapters.ShiftDaysAdapter
import com.shiftkey.codingchallenge.main.viewmodel.MainViewModel
import com.shiftkey.codingchallenge.views.RecyclerItemDivider
import kotlinx.coroutines.flow.collectLatest
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val viewModel: MainViewModel by viewModel {
        parametersOf(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initAdapter()
    }

    private fun initAdapter() {
        val shiftDaysAdapter = ShiftDaysAdapter(AndroidNavigator(this@MainActivity))
        binding.list.apply {
            adapter = shiftDaysAdapter
            setHasFixedSize(false)
            addItemDecoration(RecyclerItemDivider(this@MainActivity).itemDecor)
        }
        lifecycleScope.launchWhenCreated {
            viewModel.shifts.collectLatest {
                shiftDaysAdapter.submitData(it)
            }
        }
    }
}