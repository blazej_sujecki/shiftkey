package com.shiftkey.codingchallenge.common

import java.text.SimpleDateFormat
import java.util.*


fun formatTime(date: Date) = SimpleDateFormat("HH:mm", Locale.getDefault()).format(date)
fun formatDate(date: Date) = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date)