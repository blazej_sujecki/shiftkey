package com.shiftkey.codingchallenge.common

import android.content.Context
import android.content.Intent
import android.os.Bundle

interface Navigator {

    fun navigate(destination: Class<out Any>)
    fun navigate(destination: Class<out Any>, buildBundle: Bundle.() -> Unit = {})
    fun navigate(destination: Class<out Any>, buildBundle: Bundle.() -> Unit = {}, extras: (intent: Intent) -> Unit = {})

}

class AndroidNavigator(private val context: Context) :
        Navigator {


    override fun navigate(destination: Class<out Any>) {
        context.startActivity(Intent(context, destination))
    }

    override fun navigate(destination: Class<out Any>, buildBundle: Bundle.() -> Unit) {
        val params = Bundle().apply { buildBundle() }
        context.startActivity(Intent(context, destination).putExtras(params))
    }

    override fun navigate(destination: Class<out Any>, buildBundle: Bundle.() -> Unit, extras: (intent: Intent) -> Unit) {
        val params = Bundle().apply { buildBundle() }
        val intent = Intent(context, destination).putExtras(params)
        intent.apply(extras)
        context.startActivity(intent)
    }

}