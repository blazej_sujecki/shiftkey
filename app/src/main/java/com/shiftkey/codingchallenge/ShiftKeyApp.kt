package com.shiftkey.codingchallenge

import android.app.Application
import com.shiftkey.codingchallenge.di.KoinModule

class ShiftKeyApp: Application() {

    override fun onCreate() {
        super.onCreate()
        KoinModule.startModule(this)
    }
}