package com.shiftkey.codingchallenge.configuration

interface ConfigurationProvider {

    fun getConfiguration(): Configuration
}