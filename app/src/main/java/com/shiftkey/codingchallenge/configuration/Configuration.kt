package com.shiftkey.codingchallenge.configuration

data class Configuration( val address: String,
                          val range: Int,
                          val period: String,
                          val baseUrl: String,
                          val accept: String,
                          val contentType: String)
