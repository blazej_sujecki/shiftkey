package com.shiftkey.codingchallenge.configuration

import android.content.Context
import com.shiftkey.codingchallenge.connection.Json

class AssetConfigurationProvider(private val json: Json, private val context: Context) : ConfigurationProvider {
    override fun getConfiguration(): Configuration {
        val configurationJson = loadJsonFromAsset()
        return json.gson.fromJson(configurationJson, Configuration::class.java)
    }

    private fun loadJsonFromAsset(): String {
        var jsonToRead: String
        context.assets.open(FILE_NAME).apply {
            jsonToRead = this.readBytes().toString(Charsets.UTF_8)
        }
        return jsonToRead
    }

    private companion object {
        const val FILE_NAME = "configuration.json"
    }

}