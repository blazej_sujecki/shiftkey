package com.shiftkey.codingchallenge.di

import android.content.Context
import androidx.annotation.NonNull
import androidx.savedstate.SavedStateRegistryOwner
import com.shiftkey.codingchallenge.configuration.AssetConfigurationProvider
import com.shiftkey.codingchallenge.configuration.Configuration
import com.shiftkey.codingchallenge.connection.HttpClientFactory
import com.shiftkey.codingchallenge.connection.Json
import com.shiftkey.codingchallenge.connection.api.ApiProvider
import com.shiftkey.codingchallenge.connection.api.ShiftKeyApi
import com.shiftkey.codingchallenge.connection.converters.QueryDateConverterFactory
import com.shiftkey.codingchallenge.connection.models.Shift
import com.shiftkey.codingchallenge.details.viewmodel.DetailsViewModel
import com.shiftkey.codingchallenge.main.viewmodel.MainViewModel
import com.shiftkey.codingchallenge.main.viewmodel.MainViewModelFactory
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

class KoinModule {

    companion object {
        private val mainModule = module {
            single { Json() }
            single<Configuration> { AssetConfigurationProvider(json = get(), context = androidContext()).getConfiguration() }
            single { HttpClientFactory(json = get(), queryDateConverterFactory = QueryDateConverterFactory.create(), configuration = get()).create() }
        }

        private val viewModelModule = module {
            viewModel { (@NonNull owner: SavedStateRegistryOwner) ->
                MainViewModelFactory(owner, get<ApiProvider>().createService(ShiftKeyApi::class.java), configuration = get())
                        .create(MainViewModel::class.java) }
            viewModel { (shift: Shift) -> DetailsViewModel(shift)}
        }

        fun startModule(context: Context) {
            val list = listOf(mainModule, viewModelModule)

            startKoin {
                androidLogger()
                androidContext(context)
                modules(list)
            }
        }
    }
}