package com.shiftkey.codingchallenge.connection.converters

import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.RuntimeException
import java.lang.reflect.Type
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class QueryDateConverterFactory : Converter.Factory() {

    override fun stringConverter(type: Type, annotations: Array<out Annotation>, retrofit: Retrofit): Converter<*, String>? {
        return if (type == Date::class.java) {
            DateQueryConverter.INSTANCE as Converter<Date?, String>?
        } else super.stringConverter(type, annotations, retrofit)
    }

    private class DateQueryConverter : Converter<Date?, String?> {

        override fun convert(date: Date?): String {
            return date?.let {SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date)} ?: throw RuntimeException("can not convert date to string")
        }

        companion object {
            val INSTANCE = DateQueryConverter()
        }
    }

    companion object {
        fun create(): QueryDateConverterFactory {
            return QueryDateConverterFactory()
        }
    }
}