package com.shiftkey.codingchallenge.connection.models

import com.google.gson.annotations.JsonAdapter
import com.shiftkey.codingchallenge.connection.adapters.YyyyMmDdDateTypeAdapter
import java.util.*

data class ShiftDay(@JsonAdapter(YyyyMmDdDateTypeAdapter::class) val date: Date, val shifts: List<Shift>)