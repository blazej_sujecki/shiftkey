package com.shiftkey.codingchallenge.connection

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import retrofit2.Converter
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Type

class Json(customTypeAdapters: Map<Type, Any> = emptyMap()) {

    val gson: Gson by lazy {
        GsonBuilder().apply {
            setDateFormat(SERVER_DATE_FORMAT)
            setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
            disableHtmlEscaping()
            customTypeAdapters.forEach { (type, adapter) -> registerTypeAdapter(type, adapter) }
        }.create()
    }

    val converterFactory: Converter.Factory = GsonConverterFactory.create(gson)

    companion object {
        const val SERVER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss+HH:mm"
    }
}
