package com.shiftkey.codingchallenge.connection.models

import java.io.Serializable

data class LocalizedSpecialty(val id: Int, val specialty_id: Int, val state_id: Int, val name: String, val abbreviation: String, val specialty: Specialty): Serializable
