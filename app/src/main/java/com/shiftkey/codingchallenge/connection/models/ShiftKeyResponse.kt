package com.shiftkey.codingchallenge.connection.models

data class ShiftKeyResponse(val data: List<ShiftDay>)