package com.shiftkey.codingchallenge.connection.adapters

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import java.text.DateFormat
import java.util.*

abstract class AbstractDateTypeAdapter : TypeAdapter<Date>() {
    protected abstract val dateFormat: DateFormat

    override fun write(out: JsonWriter?, value: Date?) {
        if (out != null && value != null) {
            out.value(dateFormat.format(value))
        }
    }

    override fun read(`in`: JsonReader?): Date? {
        return `in`?.let { dateFormat.parse(it.nextString()) }
    }
}