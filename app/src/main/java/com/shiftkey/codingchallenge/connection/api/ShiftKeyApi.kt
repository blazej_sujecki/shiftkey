package com.shiftkey.codingchallenge.connection.api

import com.shiftkey.codingchallenge.connection.models.ShiftKeyResponse
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.*

interface ShiftKeyApi {

    @GET("available_shifts")
    suspend fun getShifts(
            @Query("start", encoded = true) date: Date,
            @Query("address") address: String,
            @Query("type") type: String,
            @Query("radius") radius: Int = 150
    ): ShiftKeyResponse

}