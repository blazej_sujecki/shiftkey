package com.shiftkey.codingchallenge.connection.models

import java.io.Serializable

data class Skill(val id: Int, val name: String, val color: String): Serializable