package com.shiftkey.codingchallenge.connection.models

import com.google.gson.annotations.JsonAdapter
import com.shiftkey.codingchallenge.connection.adapters.YyyyMmDdHHmmDateTypeAdapter
import java.io.Serializable
import java.util.*

data class Shift(val shift_id: String, @JsonAdapter(YyyyMmDdHHmmDateTypeAdapter::class) val start_time: Date, @JsonAdapter(YyyyMmDdHHmmDateTypeAdapter::class) val end_time: Date,
                 val normalized_start_date_time: String, val normalized_end_date_time: String,
                 val timezone: String, val covid: Boolean, val shift_kind: String,
                 val within_distance: Int, val facility_type: Facility, val skill: Skill, val localized_specialty: LocalizedSpecialty) : Serializable