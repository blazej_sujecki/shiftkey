package com.shiftkey.codingchallenge.connection.adapters

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class YyyyMmDdDateTypeAdapter: AbstractDateTypeAdapter() {
    override val dateFormat: DateFormat
        get() = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
}