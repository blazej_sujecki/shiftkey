package com.shiftkey.codingchallenge.connection

import com.shiftkey.codingchallenge.connection.api.ApiProvider
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

class HttpClient private constructor(baseUrl: String,
                                     private val converters: Set<Converter.Factory>,
                                     private val interceptors: Set<Interceptor>,
                                     private val timeout: Long) : ApiProvider {

    override fun <T> createService(type: Class<T>): T = retrofit.create(type)

    private val retrofit = Retrofit.Builder()
            .apply { if (baseUrl.isNotEmpty()) baseUrl(baseUrl) }
            .client(okHttpClient())
            .apply {
                converters.forEach {
                    addConverterFactory(it)
                }
            }
            .build()


    private fun okHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
                .readTimeout(timeout, TimeUnit.SECONDS)
                .writeTimeout(timeout, TimeUnit.SECONDS)
                .apply {
                    interceptors.forEach {
                        addInterceptor(it)
                    }
                }
                .build()
    }

    class Builder {
        private var baseUrl = ""
        private val interceptors = mutableSetOf<Interceptor>()
        private val converterFactories = mutableSetOf<Converter.Factory>()
        private var json: Json? = null
        private var timeout: Long = 30 // seconds

        fun withBaseUrl(baseUrl: String): Builder {
            this.baseUrl = baseUrl
            return this
        }

        fun withJson(json: Json): Builder {
            this.json = json
            return this
        }

        fun withTimeoutInSeconds(timeout: Long): Builder {
            this.timeout = timeout
            return this
        }

        fun withConverterFactory(converterFactory: Converter.Factory): Builder {
            converterFactories.add(converterFactory)
            return this
        }

        fun withInterceptor(interceptor: Interceptor): Builder {
            interceptors.add(interceptor)
            return this
        }


        fun build(): HttpClient {
            return HttpClient(baseUrl, converterFactories, interceptors, timeout)
        }
    }
}
