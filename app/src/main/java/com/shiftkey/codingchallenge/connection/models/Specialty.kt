package com.shiftkey.codingchallenge.connection.models

import java.io.Serializable

data class Specialty(val id: Int, val name: String, val color: String, val abbreviation: String): Serializable