package com.shiftkey.codingchallenge.connection.interceptors

import okhttp3.Interceptor
import okhttp3.Response

class ContentTypeInterceptor(private val contentType: String) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val request = original.newBuilder()
                .header("Content-Type", contentType)
                .method(original.method(), original.body())
                .build()

        return chain.proceed(request)
    }
}