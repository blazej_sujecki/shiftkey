package com.shiftkey.codingchallenge.connection.interceptors

import okhttp3.Interceptor
import okhttp3.Response

class AcceptInterceptor(private val accept: String) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val request = original.newBuilder()
                .header("Accept", accept)
                .method(original.method(), original.body())
                .build()

        return chain.proceed(request)
    }
}