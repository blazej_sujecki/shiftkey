package com.shiftkey.codingchallenge.connection.api

interface ApiProvider {
    fun <T> createService(type: Class<T>): T
}