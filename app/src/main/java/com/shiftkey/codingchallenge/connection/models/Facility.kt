package com.shiftkey.codingchallenge.connection.models

import java.io.Serializable

data class Facility(val id: Int, val name: String, val color: String): Serializable