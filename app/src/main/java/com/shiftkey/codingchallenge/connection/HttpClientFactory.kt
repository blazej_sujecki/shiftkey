package com.shiftkey.codingchallenge.connection

import com.shiftkey.codingchallenge.configuration.Configuration
import com.shiftkey.codingchallenge.connection.api.ApiProvider
import com.shiftkey.codingchallenge.connection.converters.QueryDateConverterFactory
import com.shiftkey.codingchallenge.connection.interceptors.AcceptInterceptor
import com.shiftkey.codingchallenge.connection.interceptors.ContentTypeInterceptor

class HttpClientFactory(private val json: Json,
                        private val queryDateConverterFactory: QueryDateConverterFactory,
                        private val configuration: Configuration) {

    fun create(): ApiProvider {
        return HttpClient.Builder().apply {
            withBaseUrl(configuration.baseUrl)
            withInterceptor(AcceptInterceptor(configuration.accept))
            withInterceptor(ContentTypeInterceptor(configuration.contentType))
            withConverterFactory(json.converterFactory)
            withConverterFactory(queryDateConverterFactory)
            withJson(json)
        }.build()
    }
}