package com.shiftkey.codingchallenge.views

import android.content.Context
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.DividerItemDecoration
import com.shiftkey.codingchallenge.R

class RecyclerItemDivider(context: Context) {

   val itemDecor = DividerItemDecoration(context, DividerItemDecoration.VERTICAL).apply {
       AppCompatResources.getDrawable(context, R.drawable.recycler_item_divider)?.let {
           setDrawable(it)
       }
   }
}
