package com.shiftkey.codingchallenge.views

import android.util.TypedValue
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.shiftkey.codingchallenge.common.formatTime
import com.shiftkey.codingchallenge.connection.models.Shift
import com.shiftkey.codingchallenge.main.holders.ShiftDayViewModel

@BindingAdapter(value = ["dataShift", "shiftSelector"], requireAll = true)
fun ViewGroup.data(list: List<Shift>, shiftSelectListener: ShiftDayViewModel.ShiftSelectListener) {

    removeAllViews()
    list.forEach { shift ->
        val textView = TextView(context)
        textView.apply {
            background = with(TypedValue()) {
                context.theme.resolveAttribute(
                        android.R.attr.selectableItemBackground, this, true)
                ContextCompat.getDrawable(context, resourceId)
            }
            text = createShiftsInfo(shift)
            isClickable = true
            setOnClickListener { shiftSelectListener.onSelectShift(shift) }
        }
        addView(textView)
    }
}


private fun createShiftsInfo(shift: Shift): String {
    return "${formatTime(shift.start_time)}-${formatTime(shift.end_time)}  ${shift.shift_kind}"
}


